grid = [['_', '_', '_'], ['_', '_', '_'], ['_', '_', '_']]


def print_grid(rows: list[list]):
    print('-' * 9)
    for line in rows:
        print('| ' + ' '.join(line) + ' |')
    print('-' * 9)


def make_a_step(rows, x_or_o='X') -> None:
    while True:
        coordinates = input('Enter the coordinates: ')
        try:
            numbers = [int(x) for x in coordinates.split()]
        except ValueError:
            print('You should enter numbers!')
            continue
        coordinate = tuple(numbers)
        if len(coordinate) != 2 or not 1 <= coordinate[0] <= 3 or \
                not 1 <= coordinate[1] <= 3:
            print('Coordinates should be from 1 to 3!')
            continue
        if rows[coordinate[0]-1][coordinate[1]-1] != '_':
            print('This cell is occupied! Choose another one!')
            continue
        else:
            rows[coordinate[0]-1][coordinate[1]-1] = x_or_o
            break


def check_grid(rows: list[list]):
    flags = {'X': False, 'O': False, '_': False}
    if "_" in rows[0] + rows[1] + rows[2]:
        flags["_"] = True
    combinations = [*[rows[i] for i in range(3)],
                    *[[rows[j][i] for j in range(3)] for i in range(3)],
                    [rows[i][i] for i in range(3)],
                    [rows[i][-i - 1] for i in range(3)]]
    for char in ['X', 'O']:
        for row in combinations:
            if all(char == element for element in row):
                flags[char] = True
    if flags["_"] and not flags["X"] and not flags["O"]:
        return False
    elif all((not flags["X"], not flags["O"], not flags['_'])):
        return 'Draw'
    elif all((flags["X"], not flags["O"])):
        return 'X wins'
    elif all((not flags["X"], flags["O"])):
        return 'O wins'


now = 'X'
next_ = 'O'

while True:
    print_grid(grid)
    make_a_step(grid, now)
    now, next_ = next_, now
    if result := check_grid(grid):
        print_grid(grid)
        print(result)
        break
